from typing import Generic, TypeVar, Dict, List, Optional, Any, Tuple
from abc import ABC, abstractmethod
import copy

V = TypeVar('V')  # variable type
D = TypeVar('D')  # domain type

Unassigned = "Unassigned_Var"  # a constant representing an unassigned variable


# Base class for all constraints
class Constraint(Generic[V, D], ABC):
    # The variables that the constraint is between
    def __init__(self, variables: List[V]) -> None:
        self.variables = variables

    # Must be overridden by subclasses
    @abstractmethod
    def satisfied(self, assignment: Dict[V, D]) -> bool:
        ...


class FunctionConstraint(Constraint[V, D]):
    """
    Constraint which wraps a function defining the constraint logic

    Examples:

    >>> fc1 = FunctionConstraint(lambda x, y: x == y, ["a", "b"])
    >>> fc1.satisfied({'a': 1, 'b': 1})
    True
    >>> fc1.satisfied({'a': 1, 'b': 2})
    False
    >>> fc2 = FunctionConstraint(lambda x, y, z : x + y == z, [1, 2, 3])
    >>> fc2.satisfied({1: 1, 2: 2, 3: 3})
    True
    >>> fc2.satisfied({1: 5, 2: 1, 3: 3})
    False
    """

    def __init__(self, func, variables: List[V]) -> None:
        """
        @param func: Function wrapped and queried for constraint logic
        @type  func: callable object
        @param variables: list of variables on which this constraint applies
        """
        super().__init__(variables)
        self._func = func

    def satisfied(self, assignments: Dict[V, D]) -> bool:
        parms = [assignments.get(x, Unassigned) for x in self.variables]
        missing = parms.count(Unassigned)
        if missing:
            return True
        return self._func(*parms)

    def __repr__(self):
        from dill.source import getsource
        func_string = str(getsource(self._func))
        if func_string.find('lambda') > -1:
            i = func_string.find('lambda')
            func_string = func_string[i: -1]
        return 'FunctionConstraint on ' + str(self.variables) + ' defined by \n' + func_string + '\n'


class TableConstraint(Constraint[V, D]):
    def __init__(self, variables: List[V], incompatibles_values: List[Tuple[D]]):
        super().__init__(variables)
        self.incompatibles_values = incompatibles_values

    def satisfied(self, assignment: Dict[V, D]) -> bool:
        assigned = [x for x in self.variables if x in assignment.keys()]
        for i in range(len(assigned) - 1):
            for j in range(i + 1, len(assigned)):
                t1 = (assignment[assigned[i]], assignment[assigned[j]])
                t2 = (assignment[assigned[j]], assignment[assigned[i]])
                if t1 in self.incompatibles_values or t2 in self.incompatibles_values:
                    return False
        return True

    def __repr__(self):
        return 'TableConstraint(' + str(self.variables) + '):\n ' + str(self.incompatibles_values)


class AllDifferentConstraint(Constraint[V, D]):
    """
    Constraint enforcing that values of all given variables are different
    Example:

    >>> problem = Problem(["a", "b"], {'a':[1, 2], 'b':[1, 2]})
    >>> problem.add_constraint(AllDifferentConstraint(problem.variables))
    >>> backtracking_search(problem)
    {'a': 1, 'b': 2}
    """

    def __init__(self, variables: List[V]):
        super().__init__(variables)

    def satisfied(self, assignment: Dict[V, D]) -> bool:
        assigned = [x for x in self.variables if x in assignment.keys()]
        for i in range(len(assigned) - 1):
            for j in range(i + 1, len(assigned)):
                if assignment[assigned[i]] == assignment[assigned[j]]:
                    return False
        return True

    def __repr__(self):
        return 'AllDiff(' + str(self.variables) + ')'


# A constraint satisfaction problem consists of variables of type V
# that have ranges of values known as domains of type D and constraints
# that determine whether a particular variable's domain selection is valid
class Problem(Generic[V, D]):
    def __init__(self, variables: List[V] = None, domains: Dict[V, List[D]] = None) -> None:
        self.variables: List[V] = variables  # variables to be constrained
        self.domains: Dict[V, List[List[D]]] = {}  # domain of each variable
        if domains:
            for v in domains.keys():
                self.domains[v] = [domains.get(v)]
        self.constraints: Dict[V, List[Constraint[V, D]]] = {}
        if variables:
            for variable in self.variables:
                self.constraints[variable] = []
                if variable not in self.domains:
                    raise LookupError("Every variable should have a domain assigned to it.")

    def add_variable(self, var: V, domain: D):
        if var in self.variables:
            msg = "Tried to insert duplicated variable %s" % repr(var)
            raise LookupError(msg)
        if not domain:
            raise ValueError("Domain is empty")
        self.variables.append(var)
        self.domains[var] = [domain]

    def add_constraint(self, constraint: Constraint[V, D]) -> None:
        for variable in constraint.variables:
            if variable not in self.variables:
                raise LookupError("Variable in constraint not in CSP")
            else:
                self.constraints[variable].append(constraint)

    # Check if the value assignment is consistent by checking all constraints
    # for the given variable against it
    def consistent(self, variable: V, assignment: Dict[V, D]) -> bool:
        for constraint in self.constraints[variable]:
            if not constraint.satisfied(assignment):
                return False
        return True

    def __repr__(self):
        rep = ['Variables:\n']
        the_constraints = []
        for v in self.variables:
            rep.append(str(v) + ' : ' + str(self.curr_domains(v)) + '\n')
            vc = self.constraints.get(v, [])
            the_constraints.extend([c for c in vc if c not in the_constraints])

        rep.append('Constraints:\n')
        for c in the_constraints:
            rep.append(repr(c) + '\n')
        return ''.join(rep)

    def curr_domains(self, v):
        return self.domains[v][-1]


def assign(csp, var, val, assignment):
    """Add {var: val} to assignment; Discard the old value if any.
    Do bookkeeping for the number of assignments (nassigns)."""
    if not hasattr(csp, 'nb_assigns'):
        csp.nb_assigns = 0
    csp.nb_assigns += 1
    assignment[var] = val


def unassign(csp, var, assignment):
    """Remove {var: val} from assignment; that is backtrack.
    DO NOT call this if you are changing a variable to a new value;
    just call assign for that."""
    if var in assignment:
        del assignment[var]


def backtracking_search(csp, assignment=None) -> Optional[Dict[V, D]]:
    if not hasattr(csp, "assignment_order"):
        csp.assignment_order = []

    # assignment is complete if every variable is assigned (our base case)
    if assignment is None:
        assignment = {}
    if len(assignment) == len(csp.variables):
        return assignment
        # get all variables in the CSP but not in the assignment
    unassigned: List[V] = [v for v in csp.variables if v not in assignment]
    # get the every possible domain value of the first unassigned variable
    var: V = unassigned[0]
    for value in csp.curr_domains(var)[:]:
        assign(csp, var, value, assignment)
        # if we're still consistent, we recurse (continue)
        if csp.consistent(var, assignment):
            result: Optional[Dict[V, D]] = backtracking_search(csp, assignment)
            # if we didn't find the result, we will end up backtracking
            if result is not None:
                return result
        unassign(csp, var, assignment)

    return None


# ----------------------Neighbors of a variable ----------------
# In a CSP network, the neighbors of a variable x are defined as
# the set made up of the variables sharing a constraint with x.

def vars_neighbors(csp: Problem, var):
    neighbors = set()
    if csp.constraints.get(var):
        for c in csp.constraints[var]:
            n = c.variables[:]
            n.remove(var)
            neighbors.update(n)
    return neighbors


# ----------------------END Neighbors of a variable ----------------


# --------------------Forward check algorithm -----------------------------

def forward_check(csp, var, assignment):
    """ Do forward checking (current domain reduction) for this assignment.
    @:param csp: the object csp problem
    @:param var last assigned variable
    @:param assignment the current assignment

    """
    for x in vars_neighbors(csp, var):
        if x not in assignment:
            for v in csp.curr_domains(x)[:]:
                assignment[x] = v
                if not csp.consistent(x, assignment):
                    csp.curr_domains(x).remove(v)
                    if not csp.curr_domains(x):
                        del assignment[x]
                        return False
                del assignment[x]

    return True


# --------------------END Forward check algorithm -----------------------------


# --------------------Arc consistency algorithm AC-3 -----------------------------
def every(predicate, seq):
    """True if every element of seq satisfies predicate. """
    for x in seq:
        if not predicate(x):
            return False
    return True


def revise(csp, Xi, Xj):
    " Return true if we remove a value."
    removed = False
    for vi in csp.curr_domains(Xi)[:]:
        # If Xi=vi conflicts with Xj=vj for every possible y, eliminate Xi=vi
        if every(lambda vj: not csp.consistent(Xj, {Xj: vj, Xi: vi}), csp.curr_domains(Xj)):
            csp.curr_domains(Xi).remove(vi)
            removed = True
    return removed


def AC3(csp, queue=None):
    """ Constraint Propagation with AC-3"""
    if queue is None:
        queue = [(Xi, Xk) for Xi in csp.variables for Xk in vars_neighbors(csp, Xi)]
    while queue:
        (Xi, Xj) = queue.pop()
        if revise(csp, Xi, Xj):
            if not csp.curr_domains(Xi):
                return False
            for Xk in vars_neighbors(csp, Xi):
                if Xk != Xj:  queue.append((Xk, Xi))
    return True

# -----------------------END AC-3 ----------------------------------------------------


# ----------------------Search hybridized with constraints propagation  --------------

# Do bookkeeping for the domains
def before_propagation(csp, var, assignment):
    """ Do bookkeeping for current domains : store the current domains before propagation"""
    csp.domains[var].append(csp.curr_domains(var)[:])
    csp.curr_domains(var).clear()
    csp.curr_domains(var).append(assignment.get(var))
    for v in csp.domains.keys():
        if v not in assignment.keys():
            csp.domains[v].append(csp.curr_domains(v)[:])


def restore_domains(csp,  assignment):
    """ Do bookkeeping for the domains: restore the current domains to the previous ones
     when a variable's value assignment is failed."""
    for v in csp.domains.keys():
        if v not in assignment.keys():
            del csp.domains[v][-1]


# backtracking search hybridized with constraints propagation
def backtracking_with_propagation(csp, propagation='fc', assignment=None) -> Optional[Dict[V, D]]:
    # assignment is complete if every variable is assigned (our base case)
    if assignment is None:
        assignment = {}
    if len(assignment) == len(csp.variables):
        return assignment
        # get all variables in the CSP but not in the assignment
    unassigned: List[V] = [v for v in csp.variables if v not in assignment]
    # get the every possible domain value of the first unassigned variable
    var: V = unassigned[0]
    for value in csp.curr_domains(var)[:]:
        assign(csp, var, value, assignment)
        # if we're still consistent, we recurse (continue)
        if csp.consistent(var, assignment):
            still_consistent = True
            if propagation == 'fc':
                before_propagation(csp, var, assignment)
                still_consistent = forward_check(csp, var, assignment)
            elif propagation == 'mac':
                before_propagation(csp, var, assignment)
                queue = [(Xk, var) for Xk in vars_neighbors(csp, var)]
                still_consistent = AC3(csp, queue=queue)
            if still_consistent:
                result: Optional[Dict[V, D]] = backtracking_with_propagation(csp, propagation, assignment)
                # if we didn't find the result, we will end up backtracking
                if result is not None:
                    return result
        unassign(csp, var, assignment)
        restore_domains(csp,   assignment)

    return None


def forward_checking_search(csp):
    return backtracking_with_propagation(csp, propagation='fc')


def mac_search(csp):
    return backtracking_with_propagation(csp, propagation='mac')

# ----------------------Search hybridized with constraints propagation  --------------


